require 'spec_helper'

RSpec.describe CrmReport do
  let(:lat) { 0 }
  let(:long) { 0 }
  let(:distance) { 100.0 }

  it 'has a version number' do
    expect(CrmReport::VERSION).not_to be nil
  end

  it 'print report without any error with valid data' do
    file_path = File.join('spec', 'fixtures', 'customers.json')

    expect do
      CrmReport.report_customers_in_circle(file_path, lat, long, distance)
    end.to_not(raise_error)
  end

  it 'raises error with invalid data' do
    file_path = File.join('spec', 'fixtures', 'customers_invalid.json')

    expect do
      CrmReport.report_customers_in_circle(file_path, lat, long, distance)
    end.to(raise_error(TypeError))
  end
end
