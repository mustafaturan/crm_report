require 'spec_helper'

describe CrmReport::Service::Print do
  let(:customers) do
    [
      CrmReport::ODM::Customer.new(
        user_id: 1, name: 'Must', latitude: 0.7, longitude: -5.2
      ),
      CrmReport::ODM::Customer.new(
        user_id: 2, name: 'Exist', latitude: 12.309, longitude: -3.1
      )
    ]
  end

  it 'prints customers' do
    expect(STDOUT).to receive(:puts).with('Customer Basic Info')
    expect(STDOUT).to receive(:puts).with('*' * 80)
    expect(STDOUT).to receive(:puts).with("Id \t Name")
    expect(STDOUT).to receive(:puts).with('-' * 80)
    expect(STDOUT).to receive(:puts).with(
      "#{customers[0].user_id} \t #{customers[0].name}"
    )
    expect(STDOUT).to receive(:puts).with(
      "#{customers[1].user_id} \t #{customers[1].name}"
    )
    CrmReport::Service::Print.print_customers(customers)
  end
end
