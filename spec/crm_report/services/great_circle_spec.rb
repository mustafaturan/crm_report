require 'spec_helper'

describe CrmReport::Service::GreatCircle do
  let(:latitude_from) { 53.3381985 }
  let(:longitude_from) { -6.2592576 }
  let(:latitude_to) { 53.3393 }
  let(:longitude_to) { -6.2576841 }
  let(:correct_result) { 0.16098249886386046 }

  it 'calculate distance between two locations' do
    location_from = CrmReport::ODM::Location.new(
      latitude: latitude_from,
      longitude: longitude_from
    )

    location_to = CrmReport::ODM::Location.new(
      latitude: latitude_to,
      longitude: longitude_to
    )

    great_circle_service = CrmReport::Service::GreatCircle.new(
      from: location_from,
      to: location_to
    )
    expect(great_circle_service.distance).to be correct_result
  end
end
