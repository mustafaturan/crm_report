require 'spec_helper'

describe CrmReport::Service::CustomerCollection do
  let(:latitude_to) { 53.3393 }
  let(:longitude_to) { -6.2576841 }
  let(:customer_collection) { CrmReport::Service::CustomerCollection.new }

  it 'raises error when reading customer info with non-existed-file' do
    file_path = 'non-existed-file'
    expect do
      customer_collection.read_from_file(file_path)
    end.to raise_error(IOError)
  end

  it 'raises error when reading customer info with invalid data' do
    file_path = File.join('spec', 'fixtures', 'customers_invalid.json')
    expect do
      customer_collection.read_from_file(file_path)
    end.to raise_error(TypeError)
  end

  it 'reads and import customers into customers var' do
    file_path = File.join('spec', 'fixtures', 'customers.json')
    customer_collection.read_from_file(file_path)
    number_of_customers = customer_collection.customers.length
    expect(number_of_customers).to eq(32)
  end

  it 'filters customers by distance to the given location' do
    location = CrmReport::ODM::Location.new(
      latitude: latitude_to, longitude: longitude_to
    )
    distance = 100.0
    file_path = File.join('spec', 'fixtures', 'customers.json')
    customer_collection.read_from_file(file_path)
                       .filter_by_circle(location, distance)
    number_of_customers = customer_collection.customers.length
    expect(number_of_customers).to eq(16)
  end

  it 'orders customers with user_id' do
    file_path = File.join('spec', 'fixtures', 'customers.json')
    customer_collection.read_from_file(file_path).order
    prev_id = 0
    customer_collection.customers.each do |customer|
      expect(prev_id).to be < customer.user_id
      prev_id = customer.user_id
    end
  end
end
