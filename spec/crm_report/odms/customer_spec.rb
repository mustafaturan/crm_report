require 'spec_helper'

describe CrmReport::ODM::Customer do
  let(:user_id) { 1 }
  let(:name) { 'Mustafa Turan' }
  let(:latitude) { 53.398815 }
  let(:longitude) { -6.368277 }

  it 'initializes with valid data' do
    expect(CrmReport::ODM::Location).to receive(:new).with(
      latitude: latitude, longitude: longitude
    )

    customer = CrmReport::ODM::Customer.new(
      user_id: user_id, name: name, latitude: latitude, longitude: longitude
    )

    expect(customer.user_id).to eq user_id
    expect(customer.name).to eq name
  end

  it 'raises error with missing data on initialize' do
    args = { user_id: user_id, name: name, latitude: latitude }
    expect { CrmReport::ODM::Customer.new(args) }.to(
      raise_error(ArgumentError)
    )
  end

  it 'raises error when user_id is not a positive int on initialize' do
    args = { user_id: -1, name: name, latitude: latitude, longitude: longitude }
    expect { CrmReport::ODM::Customer.new(args) }.to raise_error(TypeError)
  end

  it 'raises error when name is not string on initialize' do
    args = {
      user_id: user_id, name: nil, latitude: latitude, longitude: longitude
    }
    expect { CrmReport::ODM::Customer.new(args) }.to raise_error(TypeError)
  end
end
