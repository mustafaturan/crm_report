require 'spec_helper'

describe CrmReport::ODM::Location do
  let(:latitude_from) { 53.3381985 }
  let(:longitude_from) { -6.2592576 }
  let(:location) do
    CrmReport::ODM::Location.new(
      latitude: latitude_from, longitude: longitude_from
    )
  end

  it 'initializes with valid data' do
    expect(location.latitude).to eq(0.9309271809073008)
    expect(location.longitude).to eq(-0.10924465385047823)
  end

  it 'raises error with missing data on initialize' do
    args = {
      latitude: latitude_from
    }
    expect { CrmReport::ODM::Location.new(args) }.to(
      raise_error(ArgumentError)
    )
  end

  it 'raises error when latitude is not numeric on initialize' do
    args = {
      latitude: nil,
      longitude: longitude_from
    }
    expect { CrmReport::ODM::Location.new(args) }.to raise_error(TypeError)
  end

  it 'raises error when longitude is not numeric on initialize' do
    args = {
      latitude: latitude_from,
      longitude: ''
    }
    expect { CrmReport::ODM::Location.new(args) }.to raise_error(TypeError)
  end

  it '#distance calls GreatCircle service to calculate distance' do
    location_to = CrmReport::ODM::Location.new(
      latitude: 53.3393,
      longitude: -6.2576841
    )

    expect_any_instance_of(CrmReport::Service::GreatCircle).to receive(
      :distance
    )

    location.distance(location_to)
  end
end
