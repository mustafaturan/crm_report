require 'json'
module CrmReport
  module Service
    # Customer Collection service
    class CustomerCollection
      attr_reader :customers

      def initialize
        reset
      end

      def read_from_file(file_path)
        validate_file_existence(file_path)
        reset
        IO.foreach(file_path) { |line| process_line(line) }
        self
      end

      def filter_by_circle(location, distance)
        @customers =
          @customers.select do |customer|
            customer.location.distance(location) < distance
          end
        self
      end

      def order(order_attr = :user_id)
        @customers =
          @customers.sort_by { |customer| customer.send(order_attr) }
        self
      end

      private

      def reset
        @customers = []
        @row = 0
      end

      def process_line(line)
        @row += 1
        customer_hash = json_parse(line)
        customer = init_customer(customer_hash)
        @customers.push(customer)
      end

      def init_customer(customer_hash)
        CrmReport::ODM::Customer.new(customer_hash)
      rescue => exception
        puts "Invalid customer data format found at row ##{@row}."
        puts "--> #{exception.inspect}"
        raise exception
      end

      def validate_file_existence(file_path)
        return if File.exist?(file_path.to_s)
        raise IOError, "File not found in the given path: #{file_path}"
      end

      def json_parse(line)
        JSON.parse(line, symbolize_names: true)
      rescue
        raise IOError, "Invalid customer data format found at row ##{@row}."
      end
    end
  end
end
