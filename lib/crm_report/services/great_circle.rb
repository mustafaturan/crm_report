module CrmReport
  module Service
    # Service for calculation of great circle
    class GreatCircle
      # Radius of the earth (in KM unit)
      RADIUS = 6371.0

      def initialize(from:, to:)
        @from = from
        @to = to
      end

      # Formula: https://en.wikipedia.org/wiki/Great-circle_distance
      def distance
        Math.acos(
          sin_mult_of_latitues +
          cos_mult_of_latitues * cos_of_absolute_longitude_diff
        ) * RADIUS
      end

      private

      def sin_mult_of_latitues
        Math.sin(@from.latitude) * Math.sin(@to.latitude)
      end

      def cos_mult_of_latitues
        Math.cos(@from.latitude) * Math.cos(@to.latitude)
      end

      def cos_of_absolute_longitude_diff
        Math.cos(absolute_longitude_diff)
      end

      def absolute_longitude_diff
        (@from.longitude - @to.longitude).abs
      end
    end
  end
end
