module CrmReport
  module Service
    # Print service
    class Print
      class << self
        def print_customers(customers)
          print_customer_basic_info_headers
          customers.each { |customer| print_customer_basic_info(customer) }
        end

        def print_customer_basic_info_headers
          puts 'Customer Basic Info'
          puts '*' * 80
          puts "Id \t Name"
          puts '-' * 80
        end

        def print_customer_basic_info(customer)
          puts "#{customer.user_id} \t #{customer.name}"
        end
      end

      private_class_method :print_customer_basic_info_headers,
                           :print_customer_basic_info
    end
  end
end
