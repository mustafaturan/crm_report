module CrmReport
  module ODM
    # Location Object Data Mapper
    class Location
      attr_reader :latitude, :longitude

      def initialize(latitude:, longitude:)
        @latitude = clean_coordinate(latitude, 'latitude')
        @longitude = clean_coordinate(longitude, 'longitude')
      end

      def distance(location)
        great_circle_service = CrmReport::Service::GreatCircle.new(
          from: self, to: location
        )
        great_circle_service.distance
      end

      private

      # Degrees to radians
      def to_radians(degrees)
        degrees * Math::PI / 180.0
      end

      def clean_coordinate(degree, name)
        if float?(degree)
          to_radians(degree.to_s.to_f)
        else
          raise TypeError,
                "Invalid data for '#{name}', it must be valid "\
                "coordinate(#{name})"
        end
      end

      def float?(val)
        !val.nil? && val.to_s =~ /\A[-+]?[0-9]*\.?[0-9]+\Z/
      end
    end
  end
end
