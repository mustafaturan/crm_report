module CrmReport
  module ODM
    # Customer Object Data Mapper
    class Customer
      attr_reader :user_id, :name, :location

      def initialize(user_id:, name:, latitude:, longitude:)
        @user_id = user_id
        @name = name
        @location = CrmReport::ODM::Location.new(
          latitude: latitude, longitude: longitude
        )
        validate_data_types
      end

      private

      def validate_data_types
        validate_id
        validate_name
      end

      def validate_id
        return if @user_id.is_a?(Integer) && @user_id > 0
        raise TypeError,
              'Invalid data for \'customer user_id\', '\
              'it must be natural number.'
      end

      def validate_name
        return if @name.is_a?(String) && !@name.empty?
        raise TypeError,
              'Invalid data for \'customer name\', '\
              'it must contain at least 1 char.'
      end
    end
  end
end
