require File.dirname(__FILE__) + '/crm_report/version'
require File.dirname(__FILE__) + '/crm_report/odms/customer'
require File.dirname(__FILE__) + '/crm_report/odms/location'
require File.dirname(__FILE__) + '/crm_report/services/great_circle'
require File.dirname(__FILE__) + '/crm_report/services/customer_collection'
require File.dirname(__FILE__) + '/crm_report/services/print'

# CRM Report Project
module CrmReport
  def report_customers_in_circle(file_path, lat, long, distance)
    puts "REPORT: Customers in circle of #{distance} KM"
    location = CrmReport::ODM::Location.new(latitude: lat, longitude: long)
    customer_collection = CrmReport::Service::CustomerCollection.new
    customer_collection.read_from_file(file_path)
                       .filter_by_circle(location, distance)
                       .order
    CrmReport::Service::Print.print_customers(
      customer_collection.customers
    )
  end

  module_function(:report_customers_in_circle)
end
