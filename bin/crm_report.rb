#!/usr/bin/env ruby

require 'bundler/setup'
require 'crm_report'

file_path = ARGV[0]
lat = ARGV[1] ? ARGV[1].to_f : 53.3393
long = ARGV[2] ? ARGV[2].to_f : -6.2576841
distance = ARGV[3] ? ARGV[3].to_f : 100.0

CrmReport.report_customers_in_circle(file_path, lat, long, distance)
