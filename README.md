# CrmReport

Tiny report printer for filtering and ordering customers with in the given circle.

## Requirements

Only tested with ruby 2.4.0

## Usage

```shell
    git clone git@bitbucket.org:mustafaturan/crm_report.git

    cd crm_report

    bundle

    chmod +x bin/crm_report.rb

    bin/crm_report.rb spec/fixtures/customers.json # for default args

    # bin/crm_report.rb spec/fixtures/customers.json # lat long distance

    bin/crm_report.rb spec/fixtures/customers.json 53.3393 -6.2576841 100.0
```

## Running Tests

```shell
    cd crm_report

    rspec .
```

## License

MIT
